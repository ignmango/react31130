# Switch Game Club

La idea de este proyecto es armar una tienda para vídeo juegos de Nintendo Switch.
Hay algunas tiendas similares, se está tomando información de una ya existente. (imágenes y descripciones)

## Librerías

Este proyecto solo cuenta con las librerías expuestas en el curso de Reactjs de CoderHouse.
Lo único que esta a criterio cada alumno es la librería de estilos.

- react-materialize
- react-route-dom
- materialize-css

### `Desarrollo`

Esta entrega incluye todas las entregas anteriores hasta el momento.
No se hace énfasis en los estilos, sino que se utiliza materialize-css y se retoca muy poco. Únicamente para acomodar en cada vista.

No se utilizan promesas, estamos haciendo una llamada fetch a mocki.io retornando una lista de productos. 