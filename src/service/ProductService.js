const ProductService = () => {
    const url = "https://mocki.io/v1/3eb72864-267f-4b4c-8f43-f9d6c5e045cc";

    return {
        get: async () => await fetchData(url),
    }

}

async function fetchData(req) {
    const response = await fetch(req)
    return await response.json()
}

export {ProductService}