export const Products =
    [
        {
            id: 1,
            title: "Kirby And The Forgotten Land",
            description: "Nintendo Switch (Preventa) - ¡3 Y 12 CUOTAS SIN INTERÉS O 20% OFF EN EFECTIVO!",
            price: 12490.00,
            pictureUrl: "https://d3ugyf2ht6aenh.cloudfront.net/stores/001/316/635/products/kirbyforgotten-011-7ce6d1c49891e118e516413181622403-480-0.png",
            category: {
                id: 1,
                name: "Preventa"
            },
            stock: 10
        },
        {
            id: 2,
            title: "Power Ranger: Battle For The Grid (SUPER Edition)",
            description: "Nintendo Switch - 20% de descuento pagando con Efectivo (Solo CABA y GBA en Moto - NO aplica para OCA)",
            price: 11590.00,
            pictureUrl: "https://d3ugyf2ht6aenh.cloudfront.net/stores/001/316/635/products/powerranger1-648cc77c2391ae51e416466868043560-480-0.jpg",
            category: {
                id: 2,
                name: "Nuevos"
            },
            stock: 15
        },
        {
            id: 3,
            title: "Mario Strikers: Battle League",
            description: "Nintendo Switch (Preventa) - 20% de descuento pagando con Efectivo (Solo CABA y GBA en Moto - NO aplica para OCA)",
            price: 12490.0,
            pictureUrl: "https://d3ugyf2ht6aenh.cloudfront.net/stores/001/316/635/products/mariostrikerpreventasswitch-01-011-95b5b98a91f4b6ce2a16449577115353-480-0.png",
            category: {
                id: 1,
                name: "Preventa"
            },
            stock: 8
        },
        {
            id: 4,
            title: "Kirby And The Forgotten Land",
            description: "Nintendo Switch (Preventa) - ¡3 Y 12 CUOTAS SIN INTERÉS O 20% OFF EN EFECTIVO!",
            price: 12490.00,
            pictureUrl: "https://d3ugyf2ht6aenh.cloudfront.net/stores/001/316/635/products/kirbyforgotten-011-7ce6d1c49891e118e516413181622403-480-0.png",
            category: {
                id: 1,
                name: "Preventa"
            },
            stock: 14

        },
        {
            id: 5,
            title: "Power Ranger: Battle For The Grid (SUPER Edition)",
            description: "Nintendo Switch - 20% de descuento pagando con Efectivo (Solo CABA y GBA en Moto - NO aplica para OCA)",
            price: 11590.00,
            pictureUrl: "https://d3ugyf2ht6aenh.cloudfront.net/stores/001/316/635/products/powerranger1-648cc77c2391ae51e416466868043560-480-0.jpg",
            category: {
                id: 1,
                name: "Nuevos"
            },
            stock: 9
        },
        {
            id: 6,
            title: "Mario Strikers: Battle League",
            description: "Nintendo Switch (Preventa) 20% de descuento pagando con Efectivo (Solo CABA y GBA en Moto - NO aplica para OCA)",
            price: 12490.0,
            pictureUrl: "https://d3ugyf2ht6aenh.cloudfront.net/stores/001/316/635/products/mariostrikerpreventasswitch-01-011-95b5b98a91f4b6ce2a16449577115353-480-0.png",
            category: {
                id: 1,
                name: "Preventa"
            },
            stock: 17
        }
    ]