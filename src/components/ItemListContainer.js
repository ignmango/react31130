import {useEffect, useState} from "react";
import ItemList from "./ItemList";
import {ProductService} from "../service/ProductService";
import {useParams} from "react-router-dom";

const ItemListContainer = ({greeting}) => {

    const [items, setItems] = useState([])

    const {id: categoryDescription} = useParams();

    useEffect(() => {
        fetchData(categoryDescription);
    }, [categoryDescription])


    const fetchData = async (categoryName) => {
        let response = await ProductService().get();
        if (categoryName !== undefined) response = response.filter(item => categoryName === item.category.name)
        return setItems(response)
    }

    return (
        <>
            <h1>{greeting}</h1>
            <ItemList items={items}/>
        </>
    )
}

export default ItemListContainer;
