import './App.css';
import 'materialize-css'
import 'materialize-css/dist/css/materialize.css'
import NavBar from "./components/NavBar";
import CartWidget from "./components/CartWidget";
import ItemListContainer from "./components/ItemListContainer";
import ItemDetailContainer from "./components/ItemDetailContainer";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Cart from "./components/Cart";
import CartProvider from "./context/CartContext";

const App = () => {
    const welcomeMessage = "Item List Container";
    const messageItemDetail = "Item Detail Container";
    return (
        <BrowserRouter>
            <CartProvider>
                <NavBar>
                    <CartWidget/>
                </NavBar>
                <Routes>
                    <Route
                        path="/"
                        element={<ItemListContainer greeting={welcomeMessage}/>}/>
                    <Route
                        path="/categories/:id"
                        element={<ItemListContainer greeting={welcomeMessage}/>}/>
                    <Route
                        path="/item/:id"
                        element={<ItemDetailContainer message={messageItemDetail}/>}/>
                    <Route
                        path="/cart"
                        element={<Cart/>}/>
                </Routes>
            </CartProvider>
        </BrowserRouter>

    );
}

export default App;
